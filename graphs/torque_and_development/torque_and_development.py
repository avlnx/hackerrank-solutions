#!/bin/python3
from __future__ import annotations
import os
from dataclasses import dataclass, field
from typing import Dict, List
from enum import Enum


# Complete the roadsAndLibraries function below.
def roads_and_libraries(number_of_cities, c_lib, c_road, cities):
    graph = Graph(cities)

    # if cost of library is less than or equal to cost of a road, just build
    # a library at each node
    if c_lib <= c_road:
        return c_lib * number_of_cities

    add_disconnected_cities_nodes(number_of_cities, graph)

    number_of_clusters, number_of_edges = graph.calculate_number_of_clusters_and_edges()

    lib_in_all_cities_cost = calculate_cost_for_lib_in_all_cities(number_of_cities, c_lib)
    roads_libs_comb_cost = calculate_roads_and_libs_combination_cost(number_of_clusters, c_lib, number_of_edges, c_road)

    return min(lib_in_all_cities_cost, roads_libs_comb_cost)


def add_disconnected_cities_nodes(number_of_cities, graph):
    number_of_nodes = len(graph.nodes)
    while number_of_cities > number_of_nodes:
        # make new disconnected city nodes
        number_of_nodes += 1
        graph.add_node(number_of_nodes)


def calculate_cost_for_lib_in_all_cities(number_of_cities, library_cost):
    return number_of_cities * library_cost


def calculate_roads_and_libs_combination_cost(number_of_clusters, library_cost, number_of_edges, road_cost):
    return (number_of_clusters * library_cost) + (number_of_edges * road_cost)


class State(Enum):
    DISCONNECTED = 0
    CONNECTING = 1
    CONNECTED = 2


@dataclass
class Node:
    data: int
    neighbors: List[Node] = field(default_factory=list)
    state: State = State.DISCONNECTED

    def add_neighbor(self, node):
        self.neighbors.append(node)

    def number_of_edges(self):
        return self.count_nodes() - 1   # discount the root node's edge

    def count_nodes(self):
        nodes = 0
        if self.state == State.CONNECTED or self.state == State.CONNECTING:
            return 0
        self.state = State.CONNECTING
        for neighbor_node in self.neighbors:
            nodes += neighbor_node.count_nodes()
        self.state = State.CONNECTED    # a new edge has been made
        nodes += 1
        return nodes


class NodeFactory:
    @staticmethod
    def make_node(data):
        return Node(data=data)


@dataclass
class Graph:
    nodes: Dict[int, Node] = field(default_factory=dict)

    def __init__(self, node_pairs):
        self.nodes = {}
        for node_pair in node_pairs:
            self.__add_node_pair(node_pair)

    def add_node(self, data):
        self.nodes[data] = NodeFactory().make_node(data)

    def calculate_number_of_clusters_and_edges(self):
        number_of_clusters = 0
        number_of_edges = 0
        for node_data, node in self.nodes.items():
            if node.state == State.DISCONNECTED:
                number_of_clusters += 1
                number_of_edges += node.number_of_edges()
        return number_of_clusters, number_of_edges

    def __add_node_pair(self, data_pair):
        # get the node from index or create new one
        node_pair = []
        for data in data_pair:
            node_pair.append(self.nodes.get(data, NodeFactory().make_node(data)))

        def get_pair_node_index(current_index):
            return (current_index + 1) % 2

        for index, node in enumerate(node_pair):
            neighbor_node = node_pair[get_pair_node_index(index)]
            node.add_neighbor(neighbor_node)
            self.nodes[node.data] = node


if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    q = int(input())

    for q_itr in range(q):
        nmC_libC_road = input().split()

        n = int(nmC_libC_road[0])

        m = int(nmC_libC_road[1])

        c_lib = int(nmC_libC_road[2])

        c_road = int(nmC_libC_road[3])

        cities = []

        for _ in range(m):
            cities.append(list(map(int, input().rstrip().split())))

        result = roads_and_libraries(n, c_lib, c_road, cities)

        fptr.write(str(result) + '\n')

    fptr.close()
