from torque_and_development import Graph, roads_and_libraries, Node, NodeFactory


def test_3_is_not_5():
    assert 3 != 5


def test_roads_and_libraries_loads_correct_number_of_nodes():
    cities = [[1, 7], [1, 3], [1, 2], [2, 3], [5, 6], [6, 8]]
    graph = Graph(cities)
    assert len(graph.nodes) == 7


def test_case_1():
    cities = [[1, 7], [1, 3], [1, 2], [2, 3], [5, 6], [6, 8]]
    num_cities = 6
    c_lib = 3
    c_road = 2
    assert roads_and_libraries(num_cities, c_lib, c_road, cities) == 16


def test_case_2():
    cities = [[1, 2], [3, 1], [2, 3]]
    num_cities = 3
    c_lib = 2
    c_road = 1
    assert roads_and_libraries(num_cities, c_lib, c_road, cities) == 4


def test_case_3():
    cities = [[1, 3], [3, 4], [2, 4], [1, 2], [2, 3], [5, 6]]
    num_cities = 6
    c_lib = 2
    c_road = 5
    assert roads_and_libraries(num_cities, c_lib, c_road, cities) == 12


def test_case_4():
    # n, m, c_lib, c_road
    cities = [[1, 2], [1, 3], [1, 4]]
    num_cities = 5
    c_lib = 6
    c_road = 1
    assert roads_and_libraries(num_cities, c_lib, c_road, cities) == 15


def test_count_edges():
    node_1 = NodeFactory.make_node(1)
    node_2 = NodeFactory.make_node(2)
    node_3 = NodeFactory.make_node(3)
    node_7 = NodeFactory.make_node(7)
    node_1.neighbors = [node_2, node_7, node_3]
    node_7.neighbors = [node_1]
    node_3.neighbors = [node_1, node_2]
    node_2.neighbors = [node_1, node_3]

    assert node_1.number_of_edges() == 3
